package com.example.demo.own_annotation;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class AnnotationFinder implements BeanPostProcessor {

    @Override
    public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
        Class<?> myClass = bean.getClass();
        if (myClass.isAnnotationPresent(MyOwnAnnotation.class)) {
            log.info("MyOwnAnnotation presents in the " + beanName);
        }
        return bean;
    }
}
