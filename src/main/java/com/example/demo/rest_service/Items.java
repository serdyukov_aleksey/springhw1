package com.example.demo.rest_service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Items {
    private String favicon_url;
    private String audience;
    private String site_url;
    private String name;
}
