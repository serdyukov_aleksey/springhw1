package com.example.demo.rest_service;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Quote {
    private List<Items> items;
    public Quote() {
        items = new ArrayList<>();
    }
}
