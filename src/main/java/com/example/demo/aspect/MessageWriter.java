package com.example.demo.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Slf4j
@Component
@Aspect
public class MessageWriter {


    @Around(value = "execution(* com.example.demo.first_task.WriterOfLogs.writeData())")
    public Object writeMessage(ProceedingJoinPoint pjp) throws Throwable {
        writeBefore(pjp);
        Object retVal = pjp.proceed();
        writeAfter(pjp);
        return retVal;
    }

    private void writeBefore(ProceedingJoinPoint pjp) {
        log.info("---------------> Hello from Aspect!" + pjp.toString());
    }

    private void writeAfter(ProceedingJoinPoint pjp) {
        log.info("---------------> Bye from Aspect!" + pjp.toString());
    }
}
