package com.example.demo.async_task;

import com.example.demo.own_annotation.MyOwnAnnotation;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@MyOwnAnnotation
@Component
public class AppRunner implements CommandLineRunner {

    private final RestService restService;

    public AppRunner(RestService restService) {
        this.restService = restService;
    }

    @Override
    public void run(String... args) throws Exception {
        restService.runAsync();
    }
}
