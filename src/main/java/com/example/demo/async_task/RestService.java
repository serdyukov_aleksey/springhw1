package com.example.demo.async_task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Slf4j
@Service
public class RestService {

    private final RestTemplate restTemplate;

    private final static String urlGETList = "https://api.stackexchange.com/2.2/sites?page=1&pagesize=999&filter=!Fn4IB7S7T4v-QOAVmH9Zka8Ax*";

    public RestService(RestTemplateBuilder restTemplateBuilder) {

        this.restTemplate = restTemplateBuilder.build();
    }

    @Async
    public void runAsync() throws Exception {
        log.info("Started method runAsync");
        ResponseEntity<String> quote = restTemplate.getForEntity(urlGETList, String.class);
        log.info(quote.toString());
        Thread.sleep(1000L);
        CompletableFuture.completedFuture(quote);
    }
}
