package com.example.demo.first_task;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;

@Slf4j
@Service("writerOfLogs")
@RequiredArgsConstructor
public class WriterOfLogs {

    private final static String urlGETList = "https://api.stackexchange.com/2.2/sites?page=1&pagesize=999&filter=!Fn4IB7S7T4v-QOAVmH9Zka8Ax*";

    @Value("${val.random}")
    private String fromProp;

    private final ExternalService externalService;

    @Bean
    @PostConstruct
    public void writeData() {
        log.info("Read from properties {}", " " + fromProp);
        log.info("Read from ExternalService {}", " " + externalService.getText());
    }

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {
        return builder.build();
    }


    @Bean
    public CommandLineRunner run(RestTemplate restTemplate) throws Exception {
        return args -> {
            ResponseEntity<String> quote = restTemplate.getForEntity(urlGETList, String.class);
            log.info(quote.toString());
        };
    }
}
