package com.example.demo.first_task;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class ExternalService {
    private final String text = "Hello, Spring";
}
